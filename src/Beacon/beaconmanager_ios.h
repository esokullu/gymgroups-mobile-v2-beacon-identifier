#ifndef BEACONMANAGERAPPLE_H
#define BEACONMANAGERAPPLE_H

#include <QObject>
#include "beaconmanager_p.h"

class BeaconManager;

class BeaconManagerIos : public BeaconManagerPrivate
{
    Q_OBJECT
public:
    BeaconManagerIos(BeaconManager *q);
    void start();
    void stop();

private:
    void *m_delegate;
};

#endif // BEACONMANAGERAPPLE_H
