#include "mailmodel.h"
#include "mailmodel_p.h"

const char *MailModel::NAME = "MailModel";

MailModel::MailModel(MvcFacade *parent)
    : MvcModel(parent)
    , d_ptr(MailModelPrivate::create(this))
{
}

MailModel::~MailModel()
{
    delete d_ptr;
}

void MailModel::send(const QString &to, const QString &subject, const QString &body, bool isHTML)
{
    Q_D(MailModel);
    d->send(to, subject, body, isHTML);
}

void MailModel::init()
{
}

const char *MailModel::name()
{
    return NAME;
}

void MailModel::apply(const QVariantMap &config)
{
    Q_UNUSED(config);
}

