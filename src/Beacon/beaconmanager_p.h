#ifndef BEACONMANAGERPRIVATE_H
#define BEACONMANAGERPRIVATE_H

#include <QObject>
#include "beaconmanager.h"

class BeaconManagerPrivate : public QObject
{
    Q_OBJECT
public:
    BeaconManagerPrivate(BeaconManager *q)
        : QObject(0)
        , q_ptr(q) {}

    static BeaconManagerPrivate *createAdapter(BeaconManager *q);
    virtual void start() = 0;
    virtual void stop() = 0;

signals:
    void beaconDetectionStarted();
    void beaconReceived(const QString &id1, const QString &id2, const QString &id3, const QString &address, qreal distance);
    void beaconDetectionFinished();
    void stateChanged(const QString &state);

public slots:

private:
    Q_DECLARE_PUBLIC(BeaconManager)
    BeaconManager *q_ptr;

};

#endif // BEACONMANAGERPRIVATE_H
