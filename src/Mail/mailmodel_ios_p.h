#ifndef MAILMODEL_IOS_P_H
#define MAILMODEL_IOS_P_H

#include "mailmodel_p.h"

class MailModelIosPrivate : public MailModelPrivate
{
    Q_OBJECT
public:
    explicit MailModelIosPrivate(MailModel *q);
    void send(const QString &to, const QString &subject, const QString &body, bool isHTML = false);

private:
    void *m_delegate;
};

#endif // MAILMODEL_IOS_P_H
