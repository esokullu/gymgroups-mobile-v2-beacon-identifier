#ifndef BEACONMANAGERANDROID_H
#define BEACONMANAGERANDROID_H

#include "beaconmanager_p.h"
#include "beaconmanager.h"
#include <QDebug>

class BeaconManagerAndroid : public BeaconManagerPrivate
{
    Q_OBJECT
public:
    BeaconManagerAndroid(BeaconManager *q);
    void start();
    void stop();
};

#endif // BEACONMANAGERANDROID_H
