#ifndef APPLICATIONMODEL_H
#define APPLICATIONMODEL_H

#include <QObject>
#include <GroupsIncMvc/gmvcmodel.h>
#include <QColor>
#include <QLoggingCategory>
#include <QNetworkReply>

Q_DECLARE_LOGGING_CATEGORY(LApplicationModel)

class QNetworkAccessManager;
class QUrlQuery;
class QUrl;

class ApplicationModelPrivate : public QObject
{
    Q_OBJECT
public:
    ApplicationModelPrivate();

private:
    bool m_debugMode;
    QNetworkAccessManager *m_networkAccessManager;
    QString m_apiBaseUrl;

    friend class ApplicationModel;
};

class ApplicationModel : public MvcModel
{
    Q_OBJECT
    Q_PROPERTY(bool debugMode READ debugMode WRITE setDebugMode NOTIFY debugModeChanged)

public:
    explicit ApplicationModel(MvcFacade *parent = 0);
    ~ApplicationModel();

    static const char *NAME;

    void setDebugMode(bool debugMode);
    bool debugMode() const;

    Q_INVOKABLE void requestListApi();
    Q_INVOKABLE void saveApi(const QString &uuid, int major, int minor, const QString &name);
    Q_INVOKABLE void saveApi(const QVariantList &list);

    Q_INVOKABLE QColor colorAvg(const QColor &from, const QColor &to, qreal progress);

    void init();
    const char *name();
    void apply(const QVariantMap &config);

signals:
    void debugModeChanged();
    void database(const QVariantList &list);
    void apiError(const QString &error);
    void apiSuccess(const QString &apiPath);

public slots:
    void reply(QNetworkReply *reply);
    void error(QNetworkReply::NetworkError);

protected:
    QVariant parseJson(const QByteArray &data, bool *ok = Q_NULLPTR);
    void parseJsonRecursive(QVariantList &data, const QJsonArray &array, int depth = 0);
    void parseJsonRecursive(QVariantMap &data, const QJsonObject &object, int depth = 0);
    QUrl apiUrl(const QString &api);
    void post(const QUrl &url, const QUrlQuery &query);

private:
    Q_DECLARE_PRIVATE(ApplicationModel)
    ApplicationModelPrivate *d_ptr;

};

#endif // APPLICATIONMODEL_H
