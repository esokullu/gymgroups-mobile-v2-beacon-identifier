#ifndef MAILMODEL_P_H
#define MAILMODEL_P_H

#include <QObject>
#include "mailmodel.h"

class MailModelPrivate : public QObject
{
    Q_OBJECT
public:
    MailModelPrivate(MailModel *q)
        : QObject(0)
        , q_ptr(q) {}

    static MailModelPrivate *create(MailModel *q);

    virtual void send(const QString &to, const QString &subject, const QString &body, bool isHTML = false) = 0;

protected:
    Q_DECLARE_PUBLIC(MailModel)
    MailModel *q_ptr;
};

#endif // MAILMODEL_P_H
