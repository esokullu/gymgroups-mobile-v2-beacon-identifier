#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <GroupsIncMvc/gmvcfacade.h>
#include <GroupsIncNativeMail/gnativemailmodel.h>
#include "Beacon/beaconmodel.h"
#include "applicationmodel.h"

GMVC_DEFINE_MODEL(BeaconModel)
GMVC_DEFINE_MODEL(ApplicationModel)
GMVC_DEFINE_MODEL(NativeMailModel)

#ifdef Q_OS_IOS
extern "C" int qtmn(int argc, char **argv)
#else
int main(int argc, char *argv[])
#endif
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    MvcFacade *facade = MvcFacade::instance();
    facade->setEngine(&engine);
    facade->registerModel<BeaconModel>(new BeaconModel(facade), GMVC_MODEL(BeaconModel));
    facade->registerModel<ApplicationModel>(new ApplicationModel(facade), GMVC_MODEL(ApplicationModel));
    facade->registerModel<NativeMailModel>(new NativeMailModel(facade), GMVC_MODEL(NativeMailModel));
    facade->registerSystemModels();
    facade->bind();

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
