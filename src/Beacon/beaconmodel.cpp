#include "beaconmodel.h"

const char *BeaconModel::NAME = "BeaconModel";

BeaconModel::BeaconModel(MvcFacade *parent) :
    MvcModel(parent)
{
    qmlRegisterType<BeaconManager>(PACKAGE_NAME, 1, 0, "BeaconManager");
}

BeaconManager *BeaconModel::manager()
{
    return BeaconManager::instance();
}

void BeaconModel::init()
{
}

void BeaconModel::apply(const QVariantMap &config)
{
    Q_UNUSED(config);
}


const char *BeaconModel::name()
{
    return NAME;
}
