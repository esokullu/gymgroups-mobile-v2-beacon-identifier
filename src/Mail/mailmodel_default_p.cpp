#include "mailmodel_default_p.h"

MailModelPrivate *MailModelPrivate::create(MailModel *q)
{
    return new MailModelDefaultPrivate(q);
}

MailModelDefaultPrivate::MailModelDefaultPrivate(MailModel *q) :
    MailModelPrivate(q)
{
}

void MailModelDefaultPrivate::send(const QString &to, const QString &subject, const QString &body, bool isHTML)
{
    Q_UNUSED(to);
    Q_UNUSED(subject);
    Q_UNUSED(body);
    Q_UNUSED(isHTML);
}
