#include "applicationmodel.h"
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QUrl>
#include <QUrlQuery>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>

Q_LOGGING_CATEGORY(LApplicationModel, "GroupsIncMvcApp.ApplicationModel")

static inline int interpolate(int f, int t, qreal p)
{
    return int(f + (t - f) * p);
}

ApplicationModelPrivate::ApplicationModelPrivate()
    : QObject(0)
    , m_debugMode(false)
    , m_networkAccessManager(0)
    , m_apiBaseUrl(QString::fromUtf8(REMOTE_API_BASE_URL))
{
}

const char *ApplicationModel::NAME = "ApplicationModel";

ApplicationModel::ApplicationModel(MvcFacade *parent)
    : MvcModel(parent)
    , d_ptr(new ApplicationModelPrivate())
{
}

ApplicationModel::~ApplicationModel()
{
    delete d_ptr;
}

void ApplicationModel::setDebugMode(bool debugMode)
{
    Q_D(ApplicationModel);
    if (d->m_debugMode != debugMode) {
        d->m_debugMode = debugMode;
        emit debugModeChanged();
    }
}

bool ApplicationModel::debugMode() const
{
    Q_D(const ApplicationModel);
    return d->m_debugMode;
}

void ApplicationModel::init()
{
#if defined(DEBUG_MODE) && DEBUG_MODE == 1
    setDebugMode(true);
#endif

    Q_D(ApplicationModel);
    d->m_networkAccessManager = new QNetworkAccessManager();
    connect(d->m_networkAccessManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(reply(QNetworkReply*)));
}

void ApplicationModel::reply(QNetworkReply *reply)
{
    Q_D(ApplicationModel);

    QByteArray response = reply->readAll();

    qCDebug(LApplicationModel) << reply->url()
                               << reply->attribute(QNetworkRequest::HttpStatusCodeAttribute)
                               << reply->header(QNetworkRequest::ContentTypeHeader)
                               << response
                               << reply->errorString();

    bool ok = false;

    if (!response.isEmpty()) {
        QVariant data = parseJson(response, &ok);
        if (ok)
            emit apiSuccess(reply->url().toString().mid(d->m_apiBaseUrl.length() + 1));

        if (reply->url() == apiUrl("list") && !data.isNull())
            emit database(data.toList());

    } else if (reply->error() != QNetworkReply::NoError) {
        emit apiError("Could not connect to API.");
    }
}

QVariant ApplicationModel::parseJson(const QByteArray &data, bool *ok)
{
    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(data, &error);
    QVariant ret;

    bool gotError = true;
    if (error.error != QJsonParseError::NoError) {
        qCCritical(LApplicationModel) << "Got JSON Error: " << error.errorString();
    } else if (!doc.isObject()) {
        qCCritical(LApplicationModel) << "Invalid response.";
    } else {
        QJsonObject obj = doc.object();
        if (obj.contains(QStringLiteral("status"))
                && obj.contains(QStringLiteral("data"))
                && obj["status"].isString()) {

            if (obj["status"].toString() == QStringLiteral("ok")) {

                gotError = false;
                if (ok != Q_NULLPTR)
                    *ok = true;

                if (obj["data"].isArray()) {
                    QVariantList list;
                    parseJsonRecursive(list, obj["data"].toArray());
                    ret.setValue(list);
                } else if (obj["data"].isObject()) {
                    QVariantMap map;
                    parseJsonRecursive(map, obj["data"].toObject());
                    ret.setValue(map);
                }
            } else {
                qCDebug(LApplicationModel) << "Response Message" << obj["message"].toString();
            }
        }
    }

    if (gotError) {
        if (ok != Q_NULLPTR)
            *ok = false;

        emit apiError("API request failed.");
    }

    return ret;
}

void ApplicationModel::parseJsonRecursive(QVariantList &data, const QJsonArray &arr, int depth)
{
    if (depth > 5)
        return;

    QJsonArray array = static_cast<QJsonArray>(arr);

    int i = 0;
    int len = array.size();
    QJsonValue val;
    for (; i < len; i++) {
        val = array.at(i);
        QVariant currentVal;

        if (val.isObject()) {
            QVariantMap map;
            parseJsonRecursive(map, val.toObject(), depth + 1);
            currentVal.setValue(map);

        } else if (val.isArray()) {
            QVariantList list;
            parseJsonRecursive(list, val.toArray(), depth + 1);
            currentVal.setValue(list);
        } else if (val.isString()) {
            currentVal.setValue(val.toString());
        } else if (val.isDouble()) {
            currentVal.setValue(val.toDouble());
        } else if (val.isBool()) {
            currentVal.setValue(val.toBool());
        } else {
            qCDebug(LApplicationModel) << "Invalid data for parsing" << val;
            continue;
        }

        data.append(currentVal);
    }
}

void ApplicationModel::parseJsonRecursive(QVariantMap &data, const QJsonObject &obj, int depth)
{
    if (depth > 5)
        return;

    QJsonObject object = static_cast<QJsonObject>(obj);
    for (QJsonObject::iterator it = object.begin(); it != object.end(); ++it) {
        QJsonValue val = it.value();
        if (val.isObject()) {
            QVariantMap map;
            parseJsonRecursive(map, val.toObject(), depth + 1);
            data.insert(it.key(), map);
        } else if (val.isArray()) {
            QVariantList list;
            parseJsonRecursive(list, val.toArray(), depth + 1);
            data.insert(it.key(), list);
        } else if (val.isString()) {
            data.insert(it.key(), val.toString());
        } else if (val.isDouble()) {
            data.insert(it.key(), val.toDouble());
        } else if (val.isBool()) {
            data.insert(it.key(), val.toBool());
        } else {
            qCDebug(LApplicationModel) << "Invalid data for parsing" << it.key() << val;
        }
    }
}


void ApplicationModel::requestListApi()
{
    Q_D(ApplicationModel);
    d->m_networkAccessManager->get(QNetworkRequest(apiUrl("list")));
}

void ApplicationModel::saveApi(const QString &uuid, int major, int minor, const QString &name)
{
    QUrlQuery query;
    query.addQueryItem("uuid", uuid);
    query.addQueryItem("major", QString("%1").arg(major));
    query.addQueryItem("minor", QString("%1").arg(minor));
    query.addQueryItem("name", name);

    post(apiUrl("save"), query);
}

void ApplicationModel::saveApi(const QVariantList &list)
{
    QUrlQuery query;

    int i = 0;
    int len = list.size();

    for (; i < len; i++) {
        if (list.at(i).type() != QVariant::Map) {
            qCCritical(LApplicationModel) << "Array element should object.";
            return;
        }

        QVariantMap map = list.at(i).toMap();
        if (!map.contains("uuid")
                || !map.contains("major")
                || !map.contains("minor")
                || !map.contains("name"))
        {
            qCCritical(LApplicationModel) << "Object should contain uuid, major, minor and name fields.";
            return;
        }

        query.addQueryItem(QString("uuid[%1]").arg(i), map["uuid"].toString());
        query.addQueryItem(QString("major[%1]").arg(i), map["major"].toString());
        query.addQueryItem(QString("minor[%1]").arg(i), map["minor"].toString());
        query.addQueryItem(QString("name[%1]").arg(i), map["name"].toString());
    }

    post(apiUrl("save-bulk"), query);
}

void ApplicationModel::post(const QUrl &url, const QUrlQuery &query)
{
    Q_D(ApplicationModel);

    QNetworkRequest request;
    request.setUrl(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded; charset=UTF-8");

    QString data = query.toString(QUrl::FullyEncoded);
    qCDebug(LApplicationModel) << "Post Data" << data << request.rawHeaderList();

    QNetworkReply *reply = d->m_networkAccessManager->post(request, data.toUtf8());
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(error(QNetworkReply::NetworkError)));
}

void ApplicationModel::error(QNetworkReply::NetworkError error)
{
    QString errorStr;
    switch (error) {
    case QNetworkReply::ConnectionRefusedError:
    case QNetworkReply::RemoteHostClosedError:
    case QNetworkReply::HostNotFoundError:
    case QNetworkReply::TimeoutError:
    case QNetworkReply::OperationCanceledError:
    case QNetworkReply::SslHandshakeFailedError:
    case QNetworkReply::TemporaryNetworkFailureError:
    case QNetworkReply::NetworkSessionFailedError:
    case QNetworkReply::BackgroundRequestNotAllowedError:
    case QNetworkReply::UnknownNetworkError:
        errorStr = "Could not establish a connection.";
        break;
    default:
        qCDebug(LApplicationModel) << "Got Unhandled Network Error" << error;
    }

    if (!errorStr.isEmpty())
        apiError(errorStr);
}

QUrl ApplicationModel::apiUrl(const QString &api)
{
    Q_D(ApplicationModel);
    return QUrl(d->m_apiBaseUrl + '/' + api);
}

const char *ApplicationModel::name()
{
    return NAME;
}

void ApplicationModel::apply(const QVariantMap &config)
{
    Q_UNUSED(config);
}

QColor ApplicationModel::colorAvg(const QColor &from, const QColor &to, qreal progress)
{
    return QColor(
                qBound(0, interpolate(from.red(), to.red(), progress), 255),
                qBound(0, interpolate(from.green(), to.green(), progress), 255),
                qBound(0, interpolate(from.blue(), to.blue(), progress), 255));
}
