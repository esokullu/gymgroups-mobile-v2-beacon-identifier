#ifndef MAILMODEL_H
#define MAILMODEL_H

#include <QObject>
#include <GroupsIncMvc/gmvcmodel.h>

class MailModelPrivate;

class MailModel : public MvcModel
{
    Q_OBJECT
public:
    explicit MailModel(MvcFacade *parent = 0);
    ~MailModel();

    Q_INVOKABLE void send(const QString &to, const QString &subject, const QString &body, bool isHTML = false);

    static const char *NAME;

    void init();
    const char *name();
    void apply(const QVariantMap &config);

signals:

public slots:

private:
    Q_DECLARE_PRIVATE(MailModel)
    MailModelPrivate *d_ptr;

};

#endif // MAILMODEL_H
