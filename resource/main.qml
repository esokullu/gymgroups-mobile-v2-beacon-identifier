import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.2
import QtQuick.Controls.Private 1.0
import GroupsIncMvc 1.0
import GroupsIncMvcApp 1.0

ApplicationWindow {
    id: window

    property bool userInteraction: true
    property bool databaseListing: false

    property real initialSize: ScreenModel.dp(16)
    property real maximumSize: Math.max(width, height) * 1.2
    property int counterStart: 10
    property var deviceNames: [
        "-- Select Device Owner --",
        "[Cardio] Elliptical",
        "[Cardio] Treadmill",
        "[Cardio] Stair Climber",
        "[Cardio] Stationary Bike",

        "[Strength] Bosu Balls",
        "[Strength] Barbells",
        "[Strength] Dumbbells",
        "[Strength] V-Squat",
        "[Strength] Leg Press",
        "[Strength] Leg Extension",
        "[Strength] Iso Row",
        "[Strength] Iso Lateral Wide Chest",
        "[Strength] Iso Lateral Chest Press",
        "[Strength] Lateral Pulldown",
        "[Strength] Standing Calf",
        "[Strength] Squat",
        "[Strength] Chin/Dip Assist",

        "[Strength] Combo Cable Cross",
        "[Strength] Cable Cross",
        "[Strength] Lat Pulldown",
        "[Strength] Low Row",
        "[Strength] Glute Ham Developer",
        "[Strength] Leg Press",
        "[Strength] Leg Extension",
        "[Strength] Leg Curl",
        "[Strength] Calf Raise",
        "[Strength] Lat Raise",
        "[Strength] Row",
        "[Strength] Pulldown",
        "[Strength] Overhead Press",
        "[Strength] Arm Extension",
        "[Strength] Chest Press",
        "[Strength] Arm Curl",
        "[Strength] Back Extension",

        "[Strength] Hamstring Curl",
        "[Strength] Multi Hip",
        "[Strength] Hip Adductor & Abductor",
        "[Strength] Abdominal Crunch",
        "[Strength] Rear Delt",
        "[Strength] Pec Fly",

        "Others..."
    ]

    property var devices: [];
    property real minimumDistance: 9999999

    property Timer flashTimer: null;

    function toModel(devices) {
        var obj = Qt.createQmlObject("import QtQuick 2.3; ListModel {}", Qt.application);
        var i = 0;
        var len = devices.length;

        for (; i < len; i++)
            obj.append({ i: i, deviceName: devices[i] });

        return obj;
    }

    function delayedCall(fn, time) {
        var timer = Qt.createQmlObject("import QtQuick 2.3; Timer {}", Qt.application);
        timer.interval = time;
        timer.triggeredOnStart = false;
        timer.repeat = false;
        timer.running = false;
        timer.triggered.connect(function() {
            fn();
            timer.destroy();
        });
        timer.start();

        return timer;
    }

    function sortArrayDesc(stack, prop) {
        var len = stack.length;
        var tmp;
        var i;
        var j;

        for (i = 0; i < len; ++i) {
            for (j = i + 1; j < len; ++j) {
                if ((prop === undefined && stack[i] < stack[j]) || (prop !== undefined && stack[i][prop] < stack[j][prop])) {
                    tmp = stack[i];
                    stack[i] = stack[j];
                    stack[j] = tmp;
                }
            }
        }
    }

    function startSonar() {
        sonarItem.width = initialSize;
        sonarItem.visible = true;
        sonarAnim.start();
    }

    function stopSonar() {
        sonarAnim.stop();
        sonarItem.visible = false;
    }

    function startSearch() {
        console.log("startSearch()");

        userInteraction = false;
        minimumDistance = 9999999;

        startSonar();
        BeaconModel.manager.start();
        statusText.text = "Please wait while scanning devices";
        mainText.visible = false;
        counterText.counter = counterStart;
        counterAnimStarter.start();
    }

    function showResults() {
        console.log("showResults()");

        stopSonar();
        BeaconModel.manager.stop();

        deviceListModel.model.clear();

        var i = 0;
        var len = devices.length;
        var device;
        var found = false;

        sortArrayDesc(devices, "score");

        len = devices.length;
        for (i = 0; i < len; i++) {
            device = devices[i];
            deviceListModel.model.append(device);
            found = true;

            if (!ApplicationModel.debugMode)
                break;
        }

        if (!found)
            noResult();
        else {
            resultAnimPart1.start()
            runResultAnimPart2();
        }
    }

    function noResult() {
        userInteraction = true;
        mainText.visible = true;
        statusText.text = "Could not found any device in range.";
    }

    function runResultAnimPart2() {
        deviceList.y = window.height;
        deviceListContainer.visible = true;
        resultAnimPart2.start();
    }

    function showScan(startScan) {
        console.log("showScan()");
        userInteraction = false;
        mainText.visible = true;
        statusText.text = statusText.origText;
        showAnim.startScan = startScan === true;
        showAnim.start();
    }

    function showAnimComplete(startScan) {
        userInteraction = true;
        deviceListContainer.visible = false;
        if (startScan)
            startSearch();
    }

    function score(id1, id2, id3, distance) {
        var score = 0;
        if (distance >= 0) {
            minimumDistance = Math.min(distance, minimumDistance);
            score += 1 / distance;

            console.log("ID - Distance/Score/Minimum", id1, id2, id3, " - ", distance, score, minimumDistance);
        }

        return score;
    }

    function prepareForMail() {
        var i = 0;
        var len = devices.length;
        var device;
        var body = "";
        var name;
        var lastIndex = (deviceNames.length - 1);

        for (; i < len; i++) {
            device = devices[i];
            console.log("Mail Device Name", device.nameId, deviceNames[device.nameId]);
            if (device.nameId > 0 && deviceNames[device.nameId] !== undefined) {
                if (body === "") {
                    body = "\n\tHey Grou.ps Inc!\n\n\tCould you please register the following device(s)?\n\n";
                }

                if (device.nameId === lastIndex)
                    name = device.others;
                else
                    name = deviceNames[device.nameId];

                body += device.id1 + " (Major: " + device.id2 + ", Minor: " + device.id3 + "): " + name + "\n";
            }
        }

        if (body === "") {
            flash("You did not name any device yet.");
            return;
        }

        console.log("Mail Body: ", body);
        nativeMailModel.send("emre@groups-inc.com", "Beacon Registration Request", body);
    }

    function counterIteration(timer) {
        if (--counterText.counter === 0) {
            timer.stop();
            showResults();
        }
    }

    function flash(text) {

        if (flashTimer !== null) {
            flashTimer.stop();
            flashTimer.destroy();
            flashTimer = null;
        }

        flashContainer.visible = true;
        flashContainer.text = text;
        flashContainer.y = 0;

        flashTimer = delayedCall(function() {
            flashContainer.y = flashContainer.height * -1;
        }, 2000)
    }

    function save() {
        console.log("save()");

        var i = 0;
        var len = devices.length;
        var device;
        var name;
        var lastIndex = (deviceNames.length - 1);
        var saveList = [];

        for (; i < len; i++) {
            device = devices[i];
            console.log("Device Name", device.nameId, deviceNames[device.nameId], device.id1, device.id2, device.id3);
            if (device.nameId > 0 && deviceNames[device.nameId] !== undefined) {
                if (device.nameId === lastIndex)
                    name = device.others;
                else
                    name = deviceNames[device.nameId];
                saveList.push({
                                  uuid: device.id1,
                                  major: device.id2,
                                  minor: device.id3,
                                  name: name
                              });

            }
        }

        if (saveList.length > 0)
            ApplicationModel.saveApi(saveList);

        showScan(false);
    }

    function getDatabase() {
        console.log("getDatabase()");
        ApplicationModel.requestListApi();
    }

    visible: true
    width: 320
    height: 480

    Item {
        width: parent.width
        height: parent.height
        focus: true

        Rectangle {
            property alias text: flashContainerText.text

            id: flashContainer

            width: parent.width
            height: ScreenModel.dp(60)
            y: ScreenModel.dp(-60)
            Behavior on y {
                NumberAnimation {
                    duration: 250
                    easing.type: Easing.OutCubic
                }
            }

            color: "#FF0000"
            visible: false

            Item {
                anchors.fill: parent
                anchors.margins: ScreenModel.dp(16)
                GText {
                    id: flashContainerText
                    color: "#FFFFFF"
                    anchors.fill: parent
                    elide: Text.ElideRight
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }
            }
        }

        Item {
            property Item m_parent;

            id: devicePopup

            function show(parent) {
                console.log("Showing combobox popup.");
                m_parent = parent;
                visible = true;
            }

            function apply(index) {
                console.log("Applying combobox index.");

                if (m_parent)
                    m_parent.currentIndex = index;
                visible = false;
            }

            z: 99999
            anchors.fill: parent
            visible: false
            Rectangle {
                color: "#000"
                anchors.fill: parent
                opacity: 0.7

                MouseArea {
                    anchors.fill: parent
                    onClicked: devicePopup.visible = false
                }
            }

            Rectangle {
                id: listRoot
                color: "#5CB3E2"
                border.width: 1
                border.color: Qt.darker(color, 1.2)
                anchors.fill: parent
                anchors.margins: ScreenModel.dp(20)

                ListView {
                    anchors.fill: parent
                    clip: true
                    model: toModel(deviceNames)
                    delegate: Item {
                        width: parent.width
                        height: bottomBorder.visible ? ScreenModel.dp(41) : ScreenModel.dp(42)

                        Item {
                            anchors.left: parent.left
                            anchors.right: parent.right
                            anchors.leftMargin: ScreenModel.dp(5)
                            height: parent.height - (bottomBorder.visible ? ScreenModel.dp(1) : 0)

                            GText {
                                anchors.left: parent.left
                                anchors.right: parent.right
                                text: deviceName
                                color: "#FFF"
                                anchors.verticalCenter: parent.verticalCenter
                            }
                        }

                        Rectangle {
                            id: bottomBorder
                            height: ScreenModel.dp(1)
                            width: parent.width
                            color: Qt.darker(listRoot.color, 1.2)
                            visible: i !== deviceNames.length
                        }

                        MouseArea {
                            anchors.fill: parent
                            onClicked: devicePopup.apply(i)
                        }
                    }
                }
            }
        }

        Flickable {
            id: deviceListContainer

            anchors.fill: parent
            anchors.bottomMargin: ScreenModel.dp(100)
            contentWidth: parent.width
            contentHeight: deviceList.height
            visible: false
            clip: true
            z: 99

            Column {
                id: deviceList

                spacing: 4
                width: parent.width

                Rectangle {
                    id: indicatorTextContainer

                    color: "#FFFFFF"

                    width: parent.width
                    height: ScreenModel.dp(60)

                    GText {
                        id: indicatorText

                        font.pixelSize: ScreenModel.sp(18)
                        text: "Please name the devices"
                        color: "#000000"

                        anchors.centerIn: parent
                    }
                }

                Column {
                    spacing: 2
                    width: parent.width
                    Repeater {
                        id: deviceListModel

                        model: ListModel {}

                        delegate: Rectangle {
                            id: rowRoot

                            Component.onCompleted: console.log("Listing Item", id2, id3, nameId);

                            width: parent ? parent.width : 0
                            height: Math.max(ScreenModel.dp(80), rowColumn.height + ScreenModel.dp(20))
                            color: "#5CB3E2"

                            Column {
                                id: rowColumn

                                anchors.left: parent.left
                                anchors.right: parent.right
                                anchors.margins: ScreenModel.dp(10)

                                spacing: ScreenModel.dp(4)
                                anchors.verticalCenter: parent.verticalCenter

                                GText {
                                    anchors.left: parent.left
                                    anchors.right: parent.right
                                    color: "#FFF"
                                    font.pixelSize: ScreenModel.dp(20)
                                    text: "Nearest Beacon"
                                }

                                Rectangle {
                                    id: hiddenField

                                    color: "transparent"
                                    width: parent.width
                                    height: ApplicationModel.debugMode ? 0 : definers.height;
                                    Behavior on height {
                                        enabled: ApplicationModel.debugMode
                                        NumberAnimation {
                                            onRunningChanged: if (!running) userInteraction = true;
                                        }
                                    }

                                    clip: true

                                    Column {
                                        id: definers
                                        spacing: ScreenModel.dp(10)
                                        anchors.left: parent.left
                                        anchors.right: parent.right

                                        Rectangle {
                                            property int currentIndex: nameId !== undefined ? nameId : 0

                                            id: combobox
                                            width: parent.width
                                            height: ScreenModel.dp(40)
                                            radius: ScreenModel.dp(4)

                                            GText {
                                                anchors.left: parent.left
                                                anchors.right: parent.right
                                                anchors.leftMargin: ScreenModel.dp(5)
                                                anchors.verticalCenter: parent.verticalCenter
                                                text: deviceNames[parent.currentIndex]
                                            }

                                            MouseArea {
                                                anchors.fill: parent
                                                onClicked: devicePopup.show(parent)
                                            }

                                            onCurrentIndexChanged: if (currentIndex > 0) devices[index].nameId = currentIndex;
                                        }

                                        GTextField {
                                            visible: combobox.currentIndex === (deviceNames.length - 1)
                                            placeholderText: "Define others."
                                            onTextChanged: if (visible) devices[index].others = text;
                                        }
                                    }
                                }
                            }

                            MouseArea {
                                anchors.fill: parent
                                anchors.bottomMargin: hiddenField.height > 0 ? (hiddenField.height + rowColumn.spacing) : 0
                                enabled: userInteraction && ApplicationModel.debugMode
                                onClicked: {
                                    if (hiddenField.height === 0) {
                                        userInteraction = false;
                                        hiddenField.height = definers.height;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        Item {
            id: resultFooter
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            height: deviceListContainer.anchors.bottomMargin
            visible: deviceListContainer.visible
            opacity: 0

            Row {
                spacing: ScreenModel.dp(20)
                anchors.centerIn: parent

                FooterButton {
                    id: tryAgainButton
                    userInteraction: userInteraction
                    textField.text: "New Scan"
                    mouseArea.onClicked: showScan()
                }

                FooterButton {
                    id: saveButton
                    userInteraction: userInteraction
                    textField.text: "Save"
                    mouseArea.onClicked: save()
                }
            }
        }

        Rectangle {
            id: sonarItem
            visible: false

            border.width: 1
            border.color: "#5CB3E2"

            color: "#6d6d6d"

            opacity: 1 - ((width - initialSize) / (maximumSize - initialSize))

            width: initialSize
            height: width

            radius: width / 2

            anchors.centerIn: parent
        }

        Rectangle {
            id: button

            color: Qt.lighter("#5CB3E2", startButtonMouse.pressed ? 1.2 : 1)
            width: ScreenModel.dp(180)
            height: width
            radius: width / 2
            anchors.centerIn: parent

            GText {
                property int counter: 0

                id: counterText
                visible: !mainText.visible
                font.pixelSize: ScreenModel.sp(50)
                font.bold: true
                anchors.centerIn: parent
                text: "" + counter
                color: "#FFF"
            }

            GText {
                id: mainText
                font.pixelSize: ScreenModel.sp(50)
                font.bold: true
                anchors.centerIn: parent
                text: "Start"
                color: "#FFF"
            }

            MouseArea {
                id: startButtonMouse
                enabled: userInteraction
                anchors.fill: parent
                onClicked: startSearch()
            }
        }

        GText {
            property string origText: "Press start button for searching"

            id: statusText
            anchors.top: button.bottom
            anchors.topMargin: ScreenModel.dp(10)
            anchors.horizontalCenter: parent.horizontalCenter

            opacity: button.opacity

            font.pixelSize: ScreenModel.sp(14)
            text: origText
        }

        FooterButton {
            id: secondDbButton
            userInteraction: userInteraction
            textField.text: "Database"
            mouseArea.onClicked: getDatabase()
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: ScreenModel.dp(20)
        }

        Timer {
            id: counterAnimStarter
            interval: 1000
            repeat: true
            triggeredOnStart: false
            onTriggered: counterIteration(this)
        }

        SequentialAnimation {
            id: sonarAnim
            loops: Animation.Infinite

            PropertyAnimation {
                target: sonarItem
                properties: "width"
                from: initialSize
                to: maximumSize
                duration: 1500
            }

            PauseAnimation { duration: 1000 }
        }

        ParallelAnimation {
            id: resultAnimPart1
            PropertyAnimation {
                target: button
                properties: "opacity"
                duration: 200
                from: 1
                to: 0
            }

            PropertyAnimation {
                target: secondDbButton
                properties: "opacity"
                duration: 200
                from: 1
                to: 0
            }
        }

        SequentialAnimation {
            id: resultAnimPart2
            PropertyAnimation {
                target: deviceList
                properties: "y"
                from: window.height
                to: 0
                easing.type: Easing.OutCubic
                duration: 800
            }

            PropertyAnimation {
                target: resultFooter
                properties: "opacity"
                duration: 200
                from: 0
                to: 1
            }

            onRunningChanged: if (!running) userInteraction = true;
        }

        SequentialAnimation {
            property bool startScan: false

            id: showAnim

            PropertyAnimation {
                target: resultFooter
                properties: "opacity"
                duration: 200
                from: 1
                to: 0
            }

            PropertyAnimation {
                target: deviceList
                properties: "y"
                from: 0
                to: window.height
                easing.type: Easing.OutCubic
                duration: 800
            }

            ParallelAnimation {
                PropertyAnimation {
                    target: button
                    properties: "opacity"
                    duration: 200
                    from: 0
                    to: 1
                }

                PropertyAnimation {
                    target: secondDbButton
                    properties: "opacity"
                    duration: 200
                    from: 0
                    to: 1
                }
            }

            onRunningChanged: if (!running) showAnimComplete(startScan)
        }

        Rectangle {
            width: ScreenModel.dp(60)
            height: ScreenModel.dp(30)
            color: "#5CB3E2"
            visible: ApplicationModel.debugMode

            anchors.right: parent.right
            anchors.top: parent.top

            GText {
                font.bold: true
                anchors.centerIn: parent
                text: "Debug"
                color: "#FFF"
            }

            z: 9999
        }

        Rectangle {
            property alias model: databaseList.model

            function show() {
                visible = true;
                y = 0;
            }

            function hide() {
                y = height;
            }

            id: databaseListContainer

            z: 9999
            width: parent.width
            height: parent.height
            y: parent.height
            visible: false

            Behavior on y {
                NumberAnimation {
                    duration: 250
                    easing.type: Easing.OutCubic
                }
            }

            MouseArea {
                anchors.fill: parent
                preventStealing: false
            }

            ListView {
                id: databaseList

                clip: true
                anchors.fill: parent
                anchors.bottomMargin: ScreenModel.dp(100)
                model: ListModel {}
                spacing: ScreenModel.dp(1)
                delegate: Rectangle {

                    width: parent ? parent.width : 0
                    height: ScreenModel.dp(60)
                    color: "#5CB3E2"

                    GText {
                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.leftMargin: ScreenModel.dp(12)
                        anchors.verticalCenter: parent.verticalCenter
                        color: "#FFF"
                        font.pixelSize: ScreenModel.dp(20)
                        text: name
                    }
                }
            }

            Item {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                height: databaseList.anchors.bottomMargin

                Row {
                    spacing: ScreenModel.dp(20)
                    anchors.centerIn: parent

                    FooterButton {
                        userInteraction: userInteraction
                        textField.text: "Done"
                        mouseArea.onClicked: databaseListContainer.hide()
                    }
                }
            }
        }
    }

    Connections {
        target: ApplicationModel
        onApiSuccess: if (apiPath === 'save-bulk' || apiPath === 'save') flash("Beacons saved successfully.");

        onApiError: flash(error);

        onDatabase: {
            var len = list.length;
            if (len === 0) {
                flash("Could not found any saved devices.");
                return;
            }

            databaseListContainer.model.clear();

            for (var i = 0; i < len; i++) {
                console.log(i, list[i], list[i].uuid, list[i].name);
                databaseListContainer.model.append({
                                                       uuid: list[i].uuid,
                                                       name: list[i].name,
                                                       major: list[i].major,
                                                       minor: list[i].minor
                                                   });
            }

            databaseListContainer.show();
        }
    }

    Connections {
        target: BeaconModel.manager
        onBeaconReceived: {
            var i = 0;
            var len = devices.length;
            var device;
            var found = false;
            for (; i < len; i++) {
                device = devices[i];

                if (id1 === device.id1
                        && id2 === device.id2
                        && id3 === device.id3
                        && address === device.address) {
                    if (device.queue > 0)
                        return;

                    device.distance = distance;
                    found = true;
                    break;
                }
            }

            if (!found) {
                device = { id1: id1, id2: id2, id3: id3, address: address, distance: distance, score: 0, nameId: 0, avg: 0, inc: 0 };
                devices[len] = device;
            }

            device.score = score(id1, id2, id3, distance);
            device.distance += distance;
            device.avg = device.distance / ++device.inc;
        }
    }

    onUserInteractionChanged: console.log("Interaction Changed: ", userInteraction)
}
