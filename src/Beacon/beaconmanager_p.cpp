#include "BeaconManagerPrivate.h"

BeaconManagerPrivate *BeaconManagerPrivate::createAdapter(BeaconManager *q)
{
    return new BeaconManagerPrivate(q);
}

BeaconManagerPrivate::BeaconManagerPrivate(BeaconManager *q)
    : QObject(0)
    , q_ptr(q)
{
}

void BeaconManagerPrivate::start()
{
}
