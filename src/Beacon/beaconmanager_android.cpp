#include "beaconmanager_android.h"
#include "../jni_helper.h"

BeaconManagerPrivate *BeaconManagerPrivate::createAdapter(BeaconManager *q)
{
    return new BeaconManagerAndroid(q);
}

BeaconManagerAndroid::BeaconManagerAndroid(BeaconManager *q)
    : BeaconManagerPrivate(q)
{
}

void BeaconManagerAndroid::start()
{
    QAndroidJniObject::callStaticMethod<void>("com/ble/MainActivity", "startBeaconManager", "()V");
}

void BeaconManagerAndroid::stop()
{
}
