#ifndef BEACONMANAGERDEFAULT_H
#define BEACONMANAGERDEFAULT_H

#include <QObject>
#include "beaconmanager_p.h"

class BeaconManagerDefault : public BeaconManagerPrivate
{
    Q_OBJECT
public:
    BeaconManagerDefault(BeaconManager *q);
    void start();
    void stop();
};

#endif // BEACONMANAGERDEFAULT_H
