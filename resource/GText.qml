import QtQuick 2.3
import GroupsIncMvc 1.0

Text {
    font.pixelSize: ScreenModel.dp(16)
    font.family: "Helvetica"
    elide: Text.ElideRight
}
