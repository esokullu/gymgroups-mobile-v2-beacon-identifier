#ifndef BEACONMODEL_H
#define BEACONMODEL_H

#include <QObject>
#include <GroupsIncMvc/gmvcmodel.h>
#include "beaconmanager.h"
#include <qqml.h>

class BeaconModel : public MvcModel
{
    Q_OBJECT
    Q_PROPERTY(BeaconManager *manager READ manager CONSTANT)
public:
    explicit BeaconModel(MvcFacade *parent = 0);

    static const char *NAME;

    BeaconManager *manager();

    void init();
    const char *name();
    void apply(const QVariantMap &config);

};

QML_DECLARE_TYPE(BeaconModel)

#endif // BEACONMODEL_H
