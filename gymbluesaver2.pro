TEMPLATE = app

QT += qml quick gui gui-private network mvc nativemail

TARGET = gymbluesaver

include(deployment.pri)

PACKAGE_NAME = "\\\"GroupsInc\\\""
REMOTE_API_BASE_URL = "\\\"http://lanteans.com/groupsapi\\\""
DEBUG_MODE = 0

DEFINES += \
    PACKAGE_NAME=$$PACKAGE_NAME \
    DEBUG_MODE=$$DEBUG_MODE \
    REMOTE_API_BASE_URL=$$REMOTE_API_BASE_URL

SOURCES += \
    src/main.cpp \
    src/Beacon/beaconmanager.cpp \
    src/Beacon/beaconmodel.cpp \
    src/applicationmodel.cpp

HEADERS += \
    src/Beacon/beaconmanager.h \
    src/Beacon/beaconmanager_p.h \
    src/Beacon/beaconmodel.h \
    src/applicationmodel.h

ios {
    HEADERS += \
        src/Beacon/beaconmanager_ios.h

    OBJECTIVE_SOURCES += \
        src/Beacon/beaconmanager_ios.mm

    QMAKE_LFLAGS += \
            -framework CoreBluetooth \
            -framework CoreLocation \
            -framework Foundation

    QMAKE_INFO_PLIST = resource/ios/Info.plist
} else: android {

    QT += androidextras core-private

    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/src/android

    OTHER_FILES += \
        src/android/AndroidManifest.xml

    HEADERS += \
        src/Beacon/beaconmanager_android.h \
        src/jni_helper.h

    SOURCES += \
        src/Beacon/beaconmanager_android.cpp
} else {
    HEADERS += \
        src/Beacon/beaconmanager_default.h

    SOURCES += \
        src/Beacon/beaconmanager_default.cpp
}

RESOURCES += \
    resource/qml.qrc

