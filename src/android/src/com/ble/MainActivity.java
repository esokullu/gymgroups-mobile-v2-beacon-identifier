package com.ble;

import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import org.altbeacon.beacon.*;
import org.altbeacon.beacon.powersave.BackgroundPowerSaver;
import org.altbeacon.beacon.service.RangedBeacon;
import org.altbeacon.beacon.startup.BootstrapNotifier;
import org.altbeacon.beacon.startup.RegionBootstrap;
import org.qtproject.qt5.android.bindings.QtActivity;

import java.util.Collection;

public class MainActivity extends QtActivity implements BeaconConsumer, BootstrapNotifier, RangeNotifier
{
    private static MainActivity m_instance = null;
    protected static final String TAG = "MainActivity";

    private BeaconManager m_beaconManager;
    private Region m_region;

    @SuppressWarnings("unused")
    private BackgroundPowerSaver m_backgroundPowerSaver;

    @SuppressWarnings("unused")
    private RegionBootstrap m_regionBootstrap;

    public MainActivity()
    {
        m_instance = this;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "Activity created.");
    }

    public static MainActivity instance()
    {
        return m_instance;
    }

    public void start()
    {
        if (m_beaconManager != null) {
            Log.d(TAG, "Beacon Manager is not yet ready.");
            return;
        }

        m_beaconManager = BeaconManager.getInstanceForApplication(this);
        m_beaconManager.setBackgroundMode(false);
        // m_beaconManager.setForegroundBetweenScanPeriod(1000);
        // m_beaconManager.setForegroundScanPeriod(1000);
        m_beaconManager.setMonitorNotifier(this);

        m_region = new Region("Region", null, null, null);
        m_backgroundPowerSaver = new BackgroundPowerSaver(this);
        m_regionBootstrap = new RegionBootstrap(this, m_region);

        m_beaconManager.getBeaconParsers().add(new BeaconParser().
                setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"));

        RangedBeacon.setSampleExpirationMilliseconds(1000);

        try {
            if (!m_beaconManager.checkAvailability()) {
                Log.d(TAG, "Bluetooth is not enabled.");
            }
        } catch (BleNotAvailableException e) {
            e.printStackTrace();
        }
    }

    public static void startBeaconManager()
    {
        if (m_instance == null)
            return;

        Log.d(TAG, "Starting Manager...");
        m_instance.start();
    }

    @Override
    public void onBeaconServiceConnect() {
        Log.d(TAG, "Beacon service connected.");
        stateChanged("Beacon service connection successful");
        try {
            m_beaconManager.startMonitoringBeaconsInRegion(m_region);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void didEnterRegion(Region region) {
        Log.d(TAG, "Received: didEnterRegion");
        stateChanged("Did enter region");

        try {
            m_beaconManager.startRangingBeaconsInRegion(m_region);
            m_beaconManager.setRangeNotifier(this);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void didExitRegion(Region region) {
        Log.d(TAG, "Received: didExitRegion");

        stateChanged("Did exit region");
    }

    @Override
    public void didDetermineStateForRegion(int i, Region region) {
        Log.d(TAG, "Received: didDetermineStateForRegion");

        stateChanged("Did determine state for region");
    }

    @Override
    public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
        beaconDetectionStarted();
        for (Beacon beacon: beacons) {
            Log.d(TAG, "Beacon Received: " + beacon.toString() + " Distance: " + beacon.getDistance());
            beaconDetected(
                    beacon.getId1().toString(),
                    beacon.getId2().toString(),
                    beacon.getId3().toString(),
                    beacon.getBluetoothAddress(),
                    beacon.getDistance());
        }

        beaconDetectionFinished();
    }

    public native void beaconDetectionStarted();

    public native void beaconDetected(String id1, String id2, String id3, String bluetoothAddress, double distance);

    public native void beaconDetectionFinished();

    public native void stateChanged(String state);
}
