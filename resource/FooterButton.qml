import QtQuick 2.3
import GroupsIncMvc 1.0

Rectangle {
    property alias textField: m_textField
    property alias mouseArea: m_mouseArea
    property bool userInteraction: true

    color: Qt.lighter("#5CB3E2", m_mouseArea.pressed ? 1.2 : 1)
    width: ScreenModel.dp(80)
    height: width
    radius: width / 2

    GText {
        id: m_textField

        font.pixelSize: ScreenModel.sp(14)
        font.bold: true
        width: parent.width
        anchors.centerIn: parent
        color: "#FFF"
        horizontalAlignment: Text.AlignHCenter
    }

    MouseArea {
        id: m_mouseArea
        anchors.fill: parent
        preventStealing: true
        enabled: userInteraction && parent.opacity > 0
    }
}
