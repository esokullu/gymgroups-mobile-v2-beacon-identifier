#include "beaconmanager_default.h"

BeaconManagerPrivate *BeaconManagerPrivate::createAdapter(BeaconManager *q)
{
    return new BeaconManagerDefault(q);
}

BeaconManagerDefault::BeaconManagerDefault(BeaconManager *q)
    : BeaconManagerPrivate(q)
{
}

void BeaconManagerDefault::start()
{
}

void BeaconManagerDefault::stop()
{
}
