#include "beaconmanager_ios.h"
#include "beaconmanager_p.h"
#include "beaconmanager.h"

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface BeaconManagerAppleImpl : NSObject <CLLocationManagerDelegate>

@property (assign, nonatomic) CLLocationManager *locationManager;
@property (assign, nonatomic) NSMutableDictionary *beacons;
@property (assign, nonatomic) NSMutableArray *beaconRegions;
@property (assign, nonatomic) bool inProgress;

@end

@implementation BeaconManagerAppleImpl

- (void)startScan
{
    if (self.inProgress)
        return;

    self.inProgress = true;

    if (!self.locationManager) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;

        self.beacons = [[NSMutableDictionary alloc] init];
        self.beaconRegions = [[NSMutableArray alloc] init];

        NSArray *uuids = @[
            @"CCECC9FA-CED2-B2C4-B1D8-D3D0D3C3A3A1"
        ];

        for (NSString *uuidStr in uuids) {
            CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:uuidStr] identifier:uuidStr];
            [self.beaconRegions addObject:region];
        }
    }

    for (CLBeaconRegion *region in self.beaconRegions) {
        [self.locationManager startRangingBeaconsInRegion:region];
    }
}

- (void)stopScan
{
    if (!self.locationManager || !self.inProgress)
        return;

    for (CLBeaconRegion *region in self.beaconRegions) {
        [self.locationManager stopRangingBeaconsInRegion:region];
    }

    self.inProgress = false;
}

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region
{
    Q_UNUSED(manager);
    Q_UNUSED(region);

    int i = 0;
    int len = [beacons count];
    CLBeacon *beacon = nil;
    for (; i < len; i++) {
        beacon = [beacons objectAtIndex:i];

        emit BeaconManager::instance()->beaconReceived(
                    QString::fromNSString(beacon.proximityUUID.UUIDString),
                    QString::number([beacon.major doubleValue]),
                    QString::number([beacon.minor doubleValue]),
                    QString(),
                    beacon.accuracy);
    }
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    Q_UNUSED(manager);
    Q_UNUSED(region);

    emit BeaconManager::instance()->stateChanged("Did enter region");
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    Q_UNUSED(manager);
    Q_UNUSED(region);

    emit BeaconManager::instance()->stateChanged("Did exit region");
}

@end

BeaconManagerPrivate *BeaconManagerPrivate::createAdapter(BeaconManager *q)
{
    return new BeaconManagerIos(q);
}

BeaconManagerIos::BeaconManagerIos(BeaconManager *q)
    : BeaconManagerPrivate(q)
    , m_delegate([[BeaconManagerAppleImpl alloc] init])
{
}

void BeaconManagerIos::start()
{
    BeaconManagerAppleImpl *delegate = id(m_delegate);
    [delegate startScan];
}

void BeaconManagerIos::stop()
{
    BeaconManagerAppleImpl *delegate = id(m_delegate);
    [delegate stopScan];
}
