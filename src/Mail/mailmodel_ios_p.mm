#include "mailmodel_ios_p.h"

#include <Foundation/Foundation.h>
#include <UIKit/UIKit.h>
#include <MessageUI/MessageUI.h>
#include <qpa/qplatformnativeinterface.h>
#include <QQmlApplicationEngine>
#include <QGuiApplication>
#include <QQuickWindow>
#include <GroupsIncMvc/gmvcfacade.h>

static inline NSString *toNSString(const QString &qString)
{
    return [NSString stringWithUTF8String:qString.toUtf8().data()];
}

@interface MailControllerDelegate : UIViewController <MFMailComposeViewControllerDelegate>
{
    MFMailComposeViewController *m_mailComposer;
    MailModelIosPrivate *m_mailController;
    UIViewController *m_rootViewController;
}

@end

@implementation MailControllerDelegate

-(id)initWithMailController:(MailModelIosPrivate *)mailController andRootViewController:(UIViewController *)rootViewController
{
    self = [super init];
    if (self) {
        m_mailController = mailController;
        m_rootViewController = rootViewController;
    }

    return self;
}

-(void)send:(NSString *)to subject:(NSString *)subject body:(NSString *)body isHTML:(BOOL)isHtml
{
    m_mailComposer = [[MFMailComposeViewController alloc] init];
    m_mailComposer.mailComposeDelegate = self;

    [m_mailComposer setToRecipients:@[to]];
    [m_mailComposer setSubject:subject];
    [m_mailComposer setMessageBody:body isHTML:isHtml];

    [m_rootViewController presentModalViewController:m_mailComposer animated:YES];
    [m_mailComposer release];
}

- (void) mailComposeController:(MFMailComposeViewController *) controller didFinishWithResult:(MFMailComposeResult) result error:(NSError *) error
{
    Q_UNUSED(controller);
    Q_UNUSED(result);
    Q_UNUSED(error);
    [m_rootViewController dismissModalViewControllerAnimated:YES];
}

@end

MailModelIosPrivate::MailModelIosPrivate(MailModel *q)
    : MailModelPrivate(q)
    , m_delegate(0)
{
}

MailModelPrivate *MailModelPrivate::create(MailModel *q)
{
    return new MailModelIosPrivate(q);
}

void MailModelIosPrivate::send(const QString &to, const QString &subject, const QString &body, bool isHTML)
{
    if (!m_delegate) {
        Q_Q(MailModel);
        MvcFacade *facade = qobject_cast<MvcFacade *>(q->parent());
        QObject *top = qobject_cast<QQmlApplicationEngine *>(facade->engine())->rootObjects().at(0);
        QQuickWindow *window = qobject_cast<QQuickWindow *>(top);
        UIView *view = static_cast<UIView *>(QGuiApplication::platformNativeInterface()->nativeResourceForWindow("uiview", window));

        m_delegate = [[MailControllerDelegate alloc] initWithMailController:this andRootViewController:[[view window] rootViewController]];
    }

    MailControllerDelegate *delegate = id(m_delegate);
    [delegate send:toNSString(to) subject:toNSString(subject) body:toNSString(body) isHTML:isHTML ? YES : NO];
}
