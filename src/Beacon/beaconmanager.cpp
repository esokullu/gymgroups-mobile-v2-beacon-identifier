#include "beaconmanager.h"
#include <QDebug>
#include "beaconmanager_p.h"

BeaconManager *BeaconManager::m_instance = 0;

BeaconManager::BeaconManager(QObject *parent)
    : QObject(parent)
    , d_ptr(BeaconManagerPrivate::createAdapter(this))
{
}

BeaconManager *BeaconManager::instance()
{
    if (!m_instance)
        m_instance = new BeaconManager();

    return m_instance;
}

void BeaconManager::start()
{
    Q_D(BeaconManager);
    d->start();
}

void BeaconManager::stop()
{
    Q_D(BeaconManager);
    d->stop();
}
