#ifndef BEACONMANAGER_H
#define BEACONMANAGER_H

#include <QObject>
#include <qqml.h>

class BeaconManagerPrivate;

class BeaconManager : public QObject
{
    Q_OBJECT
public:
    explicit BeaconManager(QObject *parent = 0);

    static BeaconManager *instance();

    Q_INVOKABLE void start();
    Q_INVOKABLE void stop();

signals:
    void beaconDetectionStarted();
    void beaconReceived(const QString &id1, const QString &id2, const QString &id3, const QString &address, qreal distance);
    void beaconDetectionFinished();
    void stateChanged(const QString &state);

public slots:

private:
    static BeaconManager *m_instance;

    Q_DECLARE_PRIVATE(BeaconManager)
    BeaconManagerPrivate *d_ptr;

};

QML_DECLARE_TYPE(BeaconManager)

#endif // BEACONMANAGER_H
