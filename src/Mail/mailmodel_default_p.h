#ifndef MAILMODEL_DEFAULT_P_H
#define MAILMODEL_DEFAULT_P_H

#include "mailmodel_p.h"
#include "mailmodel.h"

class MailModelDefaultPrivate : public MailModelPrivate
{
    Q_OBJECT
public:
    MailModelDefaultPrivate(MailModel *q);
    void send(const QString &to, const QString &subject, const QString &body, bool isHTML = false);
};

#endif // MAILMODEL_DEFAULT_P_H
